package br.com.diaraujo.questao1;

public class Meteorite extends Player {

	private int meteorite_speed;

	public int getMeteoriteSpeed() {
		return meteorite_speed;
	}

	public void setMeteoriteSpeed(int meteorite_speed) {
		this.meteorite_speed = meteorite_speed;
	}
	
	@Override
	public void setPlayerX(int player_x) {
		if (player_x > 0)
			super.player_x = -player_x;
		else
			super.player_x = player_x;
	}
	
	
	@Override
	public void setPlayerY(int player_y) {
		super.player_y = 0;
	}
	
}
