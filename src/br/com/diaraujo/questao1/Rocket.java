package br.com.diaraujo.questao1;

public class Rocket extends Player {

	private int rocket_speed;

	public int getRocketSpeed() {
		return rocket_speed;
	}

	public void setRocketSpeed(int rocket_speed) {
		this.rocket_speed = rocket_speed;
	}

	@Override
	public void setPlayerY(int player_y) {
		super.player_y = 0;
	}
	
}
