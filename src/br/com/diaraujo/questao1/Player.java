package br.com.diaraujo.questao1;

public class Player {

	private String player_name;
	protected int player_x;
	protected int player_y;
	private Body player_body;
	private boolean player_visible;
	
	
	public String getPlayerName() {
		return player_name;
	}
	
	public void setPlayerName(String player_name) {
		this.player_name = player_name;
	}
	
	public int getPlayerX() {
		return player_x;
	}
	
	public void setPlayerX(int player_x) {
		this.player_x = player_x;
	}
	
	public int getPlayerY() {
		return player_y;
	}
	
	public void setPlayerY(int player_y) {
		this.player_y = player_y;
	}
	
	public Body getPlayerBody() {
		return player_body;
	}
	
	public void setPlayerBody(Body player_body) {
		this.player_body = player_body;
	}

	public boolean isPlayerVisible() {
		return player_visible;
	}

	public void setPlayerVisible(boolean player_visible) {
		this.player_visible = player_visible;
	}
	
	
}

