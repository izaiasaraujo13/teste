package br.com.diaraujo.questao2;

import java.util.ArrayList;

public class MainClass {

	public static void main(String args[]){
		
		ArrayList<Automobile> automobile_list = new ArrayList<Automobile>();
			
	
	}
	
	public void addToList(ArrayList<Automobile> list, String tag, String model, String color, int year, String owner, boolean isCar){
		
		Automobile content = new Automobile();
		content.setAutomobile_tag(tag);
		content.setAutomobile_model(model);
		content.setAutomobile_year(year);
		content.setAutomobile_owner(owner);
		content.setIs_car(isCar);
		
		list.add(content);
	}
	
	public void removeFromList(ArrayList<Automobile> list, int index){
		list.remove(index);
	}
	
	public void addToList(ArrayList<Automobile> list, Automobile content)
	{
		list.add(content);
	}
}
