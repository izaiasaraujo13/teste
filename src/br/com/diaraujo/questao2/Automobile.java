package br.com.diaraujo.questao2;

public class Automobile {

	private String	 	automobile_tag;
	private String 		automobile_model;
	private String 		automobile_color;
	private int    		automobile_year;
	private String 		automobile_owner;
	private boolean		is_car;
		
	public String getAutomobile_tag() {
		return automobile_tag;
	}
	public String getAutomobile_model() {
		return automobile_model;
	}
	public String getAutomobile_color() {
		return automobile_color;
	}
	public int getAutomobile_year() {
		return automobile_year;
	}
	public String getAutomobile_owner() {
		return automobile_owner;
	}
	public boolean isIs_car() {
		return is_car;
	}
	public void setAutomobile_tag(String automobile_tag) {
		this.automobile_tag = automobile_tag;
	}
	public void setAutomobile_model(String automobile_model) {
		this.automobile_model = automobile_model;
	}
	public void setAutomobile_color(String automobile_color) {
		this.automobile_color = automobile_color;
	}
	public void setAutomobile_year(int automobile_year) {
		this.automobile_year = automobile_year;
	}
	public void setAutomobile_owner(String automobile_owner) {
		this.automobile_owner = automobile_owner;
	}
	public void setIs_car(boolean is_car) {
		this.is_car = is_car;
	}
	
	@Override
	public String toString() {
		
		return String.format ("Automobile license plate: %s \n"+
							  "Automobile model: %s \n"+
							  "Automobile color: %s \n"+
							  "Automobile year: %d \n" +
							  "Automobile owner: %s \n"+
							  "Automobile is a car?: %b \n",
							  automobile_tag, automobile_model, automobile_color,
							  automobile_year, automobile_owner, is_car);
				
	}
	
}
